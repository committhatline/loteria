# Sorteo de la lotería de Navidad

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/SRacKOWrTyY" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

[Suscríbete al canal](https://www.youtube.com/redirect?redir_token=QUFFLUhqbGVXWTVRbUIwTjhDVS1seXEtWDNjV0d3RzVpUXxBQ3Jtc0ttREZkZWFtQVZuQ3FrSmtVM25YOEhUeVItYl9EcllGakpwN0NNbFNCTTJoUGcxRWlKNENMeEM4VlhHWEFSZktUb0hnNDRMckJNb2lRV3hVU1lmS2lHQ3BUOXJsclRRYWpnQklVM0RqQWEzbXBEX0kwOA%3D%3D&v=hLRoDs4wNCU&q=https%3A%2F%2Fbit.ly%2F3bHkIxA&event=video_description) o sígueme en [Twitter](https://twitter.com/CommitThatLine) o [Telegram](https://t.me/CommitThatLine) si quieres consejos sobre programación y alguna que otra tontería más.

En este proyecto encotnrarás 3 ficheros diferentes:

- `lotery.py` es el módulo que incluye las dos clases que utilizamos para simular la lotería: Decimo y Sorteo
- `test_lotery.py` son tests que aseguran que el módulo funciona como debe
- `navidad.ipynb` es un notebook de Jupyter en el que se puede realizar una simulación como la que vimos en el vídeo

## Descargar el código

Puedes descargar este código clonando el repositorio si te sientes cómodo usando git

```bash
git@gitlab.com:committhatline/loteria.git
```

este comando creará una carpeta llamada lotería en el direcotrio desde el que lo hayas ejectuado que contendrá el código.

Si quires, puedes descargarlo dándole al botón de descargar que puedes encontrar más arriba junto al botón de `Clonar`.

## Requisitos

Para poder ejectuar todo lo que encontrarás en este proyecto, incluídos los tests y la simulación es necesario `Python-3.8` o superior y algunos paquetes incluídos en el fichero `requirements.txt`. Para instalarlos solo tienes que ejecutar


```bash
python -m pip install -r requirements.txt
```

en el mismo directorio en el que se encuentra este proyecto. Ese comando se encargará de instalar todos los paquetes necesarios por tí y una vez que acabe podrás utilizar el código de este repositorio.

Solo tienes que abrir el proyecto con VSCode y empezar a jugar con el :)
